<?php

namespace App\Helpers;

use App\Models\Company;
use Carbon\Carbon;
use Request;

class Helper
{


    public static function formatFromDate($fromDate)
    {
        return date('Y-m-d', strtotime(\Carbon\Carbon::createFromFormat('d/m/Y', $fromDate)));
    }

    public static function formatToDate($toDate)
    {
        return date('Y-m-d', strtotime(\Carbon\Carbon::createFromFormat('d/m/Y', $toDate)) + 60 * 60 * 24);
    }
    public static function formatDate($date)
    {
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->format('d/m/Y');
    }
    public static function formatDateTime($date)
    {
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return $dt->format('H:i:s - d/m/Y');
    }
    public static function formatDate1($date)
    {
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->format('d-m-Y');
    }
    public static function setSelected($val, $origin)
    {
        $active = $val == $origin ? 'selected' : '';
        return $active;
    }
    public static function formatSqlDate($date)
    {
        if (is_null($date)) {
            return null;
        }
        $dt = new Carbon($date);
        return $dt->format('Y-m-d');
    }
    public static function indexPaginate($object, $loop)
    {
        $index = ($object->currentPage() - 1) * $object->perPage() + $loop->iteration;
        return $index;
    }
    public static function uploadSingleImage($oldImage, $name)
    {
        $image = $oldImage ? asset($oldImage) : asset('admins/images/photo.png');

        return <<<HTML
        <div class="wrap-item-image">
        <div class="preview-upload-image">
            <img id="img-sigle-preview" src="$image"/>
        </div>
        <div class="btn-photo-image mt-2 mb-2 mr-2 btn btn-success">Choose Image</div>
        <input type="file" class="form-control upload-single-image" id="upload-single-image"  name="" style="width: 0"/>
        <input type="hidden" class="form-control upload-single-image" id="value-input" value="$oldImage" name="$name"/>
        </div>
HTML;
    }


    public static function setChecked($val, $origin)
    {
        $active = $val == $origin ? 'checked' : '';
        return $active;
    }


    public static function formatDateChat($date)
    {
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->format('d-m-Y H:i');
    }
    public static function formatDateDiffForHumans($date)
    {
        $now = Carbon::now();
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->diffForHumans($now);
    }
    public static function formatDateNew($date)
    {
        if (is_null($date)) {
            return null;
        } else {
            $dt = new Carbon($date);
            return  $dt->format('d-m-Y');
        }
    }
    public static function active_menusub($arrControler)
    {
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
        list($controller, $action) = explode('@', $controller);
        if (in_array($controller, $arrControler)) {
            return "active";
        }
        return "";
    }
    public static function active_menu($arrControler)
    {
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
        list($controller, $action) = explode('@', $controller);
        if ($controller == $arrControler) {
            return "active";
        }
        return "";
    }
    public static function active_show($arrControler)
    {
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
        list($controller, $action) = explode('@', $controller);
        if (in_array($controller, $arrControler)) {
            return "mm-active mm-collapse mm-show";
        }
        return "";
    }
    public static function showCategory($level)
    {
        switch ($level) {
            case "1":
                echo "";
                break;
            case "2":
                echo "--";
                break;
            case "3":
                echo "------";
                break;
            case "4":
                echo "------------";
                break;
            case "5":
                echo "----------------------";
                break;
            case "6":
                echo "----------------------------------";
                break;
            default:
                echo "";
        }
    }
}
