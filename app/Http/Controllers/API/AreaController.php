<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Http\Requests\AreaCreateRequest;
use App\Http\Requests\AreaListRequest;
use App\Http\Requests\AreaUpdateRequest;
use App\Http\Resources\AreaResource;
use App\Models\Area;

class AreaController extends ApiController
{

    public function list(AreaListRequest $request)
    {
        $areaName = $request->name;
        $shipFeeFrom = $request->ship_fee_from;
        $shipFeeTo = $request->ship_fee_to;
        $areas  = Area::orderBy('id', 'DESC');
        if (!is_null($areaName)) {
            $areas  = $areas->where('name', 'LIKE', '%' . $areaName . '%');
        }
        if (!is_null($shipFeeFrom)) {
            $areas  = $areas->where('ship_fee', '>=', $shipFeeFrom);
        }
        if (!is_null($shipFeeTo)) {
            $areas = $areas->where('ship_fee', '<=', $shipFeeTo);
        }
        return $this->formatCollectionJson(AreaResource::class, $areas->paginate(config('setting.paginate')));
    }

    public function create(AreaCreateRequest $request)
    {
        try {
            $data = $request->getData();
            $area = Area::create($data);
            return $this->formatJson(AreaResource::class, $area);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function show($id)
    {
        $data = Area::findOrFail($id);
        return $this->formatJson(AreaResource::class, $data);
    }

    public function update(AreaUpdateRequest $request, $id)
    {
        $data = $request->getData();
        $area = Area::findOrFail($id);
        $area->update($data);
        return $this->formatJson(AreaResource::class, $area);
    }

    public function destroy($id)
    {
        $area = Area::findOrFail($id);
        $area->delete();
        return $this->sendMessage([trans('message.area.success.delete')], 200);
    }
}
