<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->getData();
        $data['is_admin'] = 0;
        $data['password'] = Hash::make($data['password']);
        $data['wallet'] = 0;
        $user = User::create($data);
        return $this->formatJson(UserResource::class, $user);
    }

    public function login(LoginRequest $request)
    {

        try {
            $credentials['email'] = $request->input('email');
            $credentials['password'] = $request->input('password');
            $token = null;

            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => trans('message.login.error')], 422);
            }

            $user = auth()->user();
            $user = $user->setAttribute('token', $token);
            return $this->successResponse(['message' => trans('message.login.success'), 'data' => new UserResource($user)], 200);
        } catch (JWTException $e) {
            return response()->json(['message' => "Error"], 500);
        }
    }

    public function getProfile()
    {
        $user = auth()->user();
        return $this->formatJson(UserResource::class, $user);
    }
    public function updateProfile(UserUpdateRequest $request)
    {
        $user = auth()->user();
        $data = $request->getData();
        $user->update($data);
        return $this->formatJson(UserResource::class, $user);
    }
}
