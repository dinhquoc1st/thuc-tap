<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Http\Requests\DiscountCreateRequest;
use App\Http\Requests\DiscountListRequest;
use App\Http\Requests\DiscountUpdateRequest;
use App\Http\Resources\DiscountResource;
use App\Models\DiscountCode;


class DiscountController extends ApiController
{

    public function list(DiscountListRequest $request)
    {
        $discountCode = $request->code;
        $amountFrom = $request->amount_from;
        $amountTo = $request->amount_to;
        $discounts  = DiscountCode::orderBy('id', 'DESC');
        if (!is_null($discountCode)) {
            $discounts  = $discounts->where('code', 'LIKE', '%' . $discountCode . '%');
        }
        if (!is_null($amountFrom)) {
            $discounts  = $discounts->where('amount', '>=', $amountFrom);
        }
        if (!is_null($amountTo)) {
            $discounts = $discounts->where('amount', '<=', $amountTo);
        }
        return $this->formatCollectionJson(DiscountResource::class, $discounts->paginate(config('setting.paginate')));
    }

    public function create(DiscountCreateRequest $request)
    {
        try {
            $data = $request->getData();
            $area = DiscountCode::create($data);
            return $this->formatJson(DiscountResource::class, $area);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function show($id)
    {
        $data = DiscountCode::findOrFail($id);
        return $this->formatJson(DiscountResource::class, $data);
    }

    public function update(DiscountUpdateRequest $request, $id)
    {
        $data = $request->getData();
        $area = DiscountCode::findOrFail($id);
        $area->update($data);
        return $this->formatJson(DiscountResource::class, $area);
    }

    public function destroy($id)
    {
        $area = DiscountCode::findOrFail($id);
        $area->delete();
        return $this->sendMessage([trans('message.discount_code.success.delete')], 200);
    }
}
