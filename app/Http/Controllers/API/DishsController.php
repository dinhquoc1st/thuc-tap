<?php

namespace App\Http\Controllers\API;

use App\Enums\UserRole;
use App\Http\Controllers\ApiController;
use App\Http\Requests\DishCreateRequest;
use App\Http\Requests\DishListRequest;
use App\Http\Requests\DishsRequest;
use App\Http\Requests\DishUpdateRequest;
use App\Http\Resources\DishResource;
use App\Http\Resources\DishsResource;
use App\Http\Resources\DishsResourceCollection;
use App\Models\Dish;
use App\Models\Store;

class DishsController extends ApiController
{

    public function list(DishListRequest $request)
    {
        $dishName = $request->name;
        $nameStore = $request->name_store;
        $dishs  = Dish::orderBy('id', 'DESC');
        if (!is_null($dishName)) {
            $dishs  = $dishs->where('name', 'LIKE', '%' . $dishName . '%');
        }
        if(!is_null($nameStore)) {
            $dishs = $dishs->whereHas('store',function ($q) use ($nameStore) {
            $q->where('name', 'LIKE', '%' . $nameStore . '%');
            });
        }
        return $this->formatCollectionJson(DishsResource::class, $dishs->paginate(config('setting.paginate')));
    }

    public function store(DishCreateRequest $request)
    {
        $dish = Dish::create($request->getData());
        return $this->formatJson(DishsResource::class, $dish);
    }

    public function show($id)
    {
        $data = Dish::find($id);
        return $this->formatJson(DishsResource::class, $data);
    }

    public function update(DishUpdateRequest $request, $id)
    {
        $data = $request->getData();
        $dish = Dish::findOrFail($id);
        $dish->update($data);
        return $this->formatJson(DishsResource::class, $dish);
    }

    public function destroy($id)
    {
        $dish = Dish::findOrFail($id);
        $dish->delete();
        return $this->sendMessage([trans('message.dish.success.delete')], 200);
    }
}
