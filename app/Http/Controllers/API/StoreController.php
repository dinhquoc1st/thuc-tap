<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\StoreCreateRequest;
use App\Http\Requests\StoreListRequest;
use App\Http\Requests\StoreUpdateRequest;
use App\Http\Resources\StoreResource;
use App\Http\Resources\StoreResourceCollection;
use App\Models\Dish;
use App\Models\Store;


class StoreController extends ApiController
{
    public function list(StoreListRequest $request)
    { 
        $storeName = $request->name;
        $nameArea = $request->name_area;
        $stores  = Store::orderBy('id', 'DESC');
        if (!is_null($storeName)) {
            $stores  = $stores->where('name', 'LIKE', '%' . $storeName . '%');
        }
        if(!is_null($nameArea)) {
            $stores = $stores->whereHas('area',function ($q) use ($nameArea) {
            $q->where('name', 'LIKE', '%' . $nameArea . '%');
            });
        }
        return $this->formatCollectionJson(StoreResource::class, $stores->paginate(config('setting.paginate')));
    }

    public function create(StoreCreateRequest $request)
    {
        $store = Store::create($request->getData());
        return $this->formatJson(StoreResource::class, $store);
    }

    public function show($id)
    {
        $data = Store::findOrFail($id);
        return $this->formatJson(StoreResource::class, $data);
    }

    public function update(StoreUpdateRequest $request, $id)
    {
        $data = $request->getData();
        $store = Store::findOrFail($id);
        $store->update($data);
        return $this->formatJson(StoreResource::class, $store);
    }

    public function destroy($id)
    {
        $area = Store::findOrFail($id);
        $area->delete();
        return $this->sendMessage([trans('message.discount_code.success.delete')], 200);
    }
}
