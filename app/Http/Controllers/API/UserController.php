<?php

namespace App\Http\Controllers\API;

use App\Enums\UserRole;
use App\Helpers\Helper;
use App\Http\Requests\AdminRegisterRequest;
use App\Http\Requests\AdminUpdateUserRequest;
use App\Http\Requests\SearchNameUserOrder;
use App\Http\Requests\UserDishRequest;
use App\Http\Resources\DishResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserOrderResourceCollection;
use App\Http\Resources\UserResource;
use App\Models\Dish;
use App\Models\User;
use App\Models\UserDish;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;




use Illuminate\Support\Facades\Hash;


class UserController extends ApiController
{
    public function create(AdminRegisterRequest $request)
    {
        $data = $request->getData();
        $data['password'] = Hash::make(config('setting.password_default'));        
        $data['is_admin'] = UserRole::MEMBER;
        $data['wallet'] = 0;
        $user = User::create($data);
        return $this->formatJson(UserResource::class, $user);
    
    }

    public function show(Request $request)
    {
        $admin = auth()->user();
        $perPage = $request->per_page;
        $perPage = $perPage ? $perPage : config('setting.paginate');
        if ($admin->is_admin === UserRole::ADMIN) {
            $data = User::paginate(2);
            return $this->formatJson(UserCollection::class, $data);
        } else {
            return $this->errorResponse('Lấy dữ liệu không thành công');
        }
    }

    public function getProfile($id)
    {
        $findUser = User::find($id);
        return $this->formatJson(UserResource::class, $findUser);
    }

    public function update(AdminUpdateUserRequest $request, $id)
    {
        $admin = auth()->user();
        $data = $request->getData();
        if ($admin->is_admin === UserRole::ADMIN) {
            $infoUser =  User::findOrFail($id)->update($data);
            $infoUser = User::find($request->id);
            return $this->sendMessageComment('Cập nhật thông tin user thành công', $infoUser);
        } else {
            return $this->errorResponse('Cập nhật thông tin thất bại');
        }
    }

    public function delete($id)
    {
        $admin = auth()->user();
        if ($admin->role === UserRole::ADMIN) {
            $user = User::find($id);
            $user->delete();
            return $this->sendMessage('Xóa tài khoản thành công');
        } else {
            return $this->errorResponse('Xóa tài khoản không thành công');
        }
    }

    public function userCallDishs(UserDishRequest $request)
    {
        $user = auth()->user();
        $dishId = Dish::findOrFail($request->dish_id);
        $totalAmount = $dishId->price * $request->number_dish;
        $userCallDish = UserDish::create([
            'user_id' => $user->id,
            'dish_id' => $dishId->id,
            'number_dish' => $request->number_dish,
            'total_amount' => $totalAmount
        ]);
        return $this->formatJson(DishResource::class, $userCallDish);
    }

    public function searchUserOrder(SearchNameUserOrder $request)
    {
        $user_data = User::where('name', 'like', '%' . $request->name . '%')->first();
        $user_order = UserDish::where('user_id', $user_data->id)->get();
        return $this->formatJson(UserOrderResourceCollection::class, $user_order);
    }

    public function showUserOrder(Request $request)
    {
        $user = auth()->user();
        $fromDate = $request->date_start;
        $toDate = $request->date_end;
        $userDishs = UserDish::where('user_id', $user->id);

        if (!empty($fromDate)) {
            $from = Helper::formatFromDate($fromDate);
            $userDishs = $userDishs->where('created_at', '>=', $from);
        }
        if (!empty($toDate)) {
            $to = Helper::formatToDate($toDate);
            $userDishs = $userDishs->where('created_at', '<=', $to);
        }
        return $this->formatJson(UserOrderResourceCollection::class, $userDishs->get());
    }
    
    public function totalOrderMonth()
    {
        $arr = [];
        for ($i = 1; $i <= 12; $i++) {


            $totalOrder = UserDish::whereMonth('created_at', $i);

            $arr[] = [
                'month' => is_null($totalOrder) ? 0 : $i,
                'total' => $totalOrder->sum('total_amount')
            ];
        }

        return $this->sendSuccessList($arr);
    }
}
