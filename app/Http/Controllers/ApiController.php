<?php

namespace App\Http\Controllers;



class ApiController extends Controller
{

    protected function presenterPostJson($item, $code = 201)
    {
        return response()->json($item->presenter(), $code);
    }

    protected function sendResponse($resource)
    {
        return $resource->response()->setStatusCode(200);
    }

    protected function sendError404($message)
    {
        return response()->json(['message' => $message], 404);
    }
    protected function sendError400($resource, $message)
    {
        $data = [
            'message' => $message,
            'errors' => $resource,
        ];
        return response()->json($data, 400);
    }

    protected function formatJson($class_name, $item, $other = null)
    {
        return new $class_name($item, $other);
    }

    protected function formatCollectionJson($class_name, $item)
    {
        return $class_name::collection($item);
    }

    protected function sendMessage($message, $code = 200)
    {
        return response()->json(['message' => $message], $code);
    }

    protected function sendMessageComment($message, $value, $code = 201)
    {
        $data = [
            "message" => $message,
            "data" => $value
        ];
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code = 400)
    {
        return response()->json(['message' => $message], $code);
    }

    protected function sendError403($message)
    {
        return response()->json(['message' => $message], 403);
    }

    protected function sendSuccessList($data)
    {
        return response()->json(['data' => $data], 200);
    }
    
    public function successResponse($data, $code = 200)
    {
        return response()->json($data, $code);
    }
    
}
