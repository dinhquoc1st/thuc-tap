<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminUpdateUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|max:255',
            'phone' => 'nullable',
            'address' => 'nullable',
            'gender' => 'nullable',
            'email' => 'nullable|regex:/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/|unique:users,email,'.$this->id,
            'password' => 'nullable',
            'is_admin' => 'nullable|0,1',
            'wallet' => 'nullable'
        ];
    }
    public function getData()
    {
        $data = $this->only(['name','phone','address','email','password','gender','is_admin','wallet']);
        return $data;
    }
}
