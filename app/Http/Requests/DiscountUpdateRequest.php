<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiscountUpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'nullable|max:255|unique:discount_code,code,'. $this->id,
            'amount' => 'nullable|integer'
        ];
    }
    public function getData()
    {
        $data = $this->only(['code','amount']);
        return $data;
    }
}
