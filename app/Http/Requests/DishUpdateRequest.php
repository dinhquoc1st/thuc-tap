<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DishUpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'nullable|max:255|unique:dishs,name,'. $this->id,
            'price' => 'nullable|integer',
            'store_id' => 'nullable|integer|exists:stores,id'
        ];
    }
    public function getData()
    {
        $data = $this->only(['name','price','store_id']);
        return $data;
    }

}
