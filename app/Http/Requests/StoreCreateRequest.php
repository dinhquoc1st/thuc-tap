<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCreateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:stores',
            'address' => 'required|max:255',
            'area_id' => 'required|integer|exists:areas,id',
            'type' => 'required|in:1,2',
            'time_start' => 'required|integer|min:1|max:24',
            'time_end' => 'required|integer|min:1|max:24'
        ];
    }
    public function getData()
    {
        $data = $this->only(['name','address','area_id','type','time_start','time_end']);
        return $data;
    }
    
}
