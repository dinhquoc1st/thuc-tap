<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255|unique:stores,name,'. $this->id,
            'address' => 'nullable|max:255',
            'area_id' => 'nullable|integer|exists:areas,id',
            'type' => 'nullable|integer|in:1,2',
            'time_start' => 'nullable|integer|min:1|max:24',
            'time_end' => 'nullable|integer|min:1|max:24'
        ];
    }
    public function getData()
    {
        $data = $this->only(['name','address','area_id','type','time_start','time_end']);
        return $data;
    }
}
