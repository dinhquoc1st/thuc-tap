<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable',
            'phone' => 'nullable',
            'address' => 'nullable',
            'gender' => 'nullable',
            'password' => 'nullable',
            'email' => 'nullable|email|unique:users,email,'. auth()->id(),
        ];
    }
    public function getData()
    {
        $data = $this->only(['name','phone','address','password','gender','email']);
        return $data;
    }
}
