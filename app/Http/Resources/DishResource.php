<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\UserDish;
class DishResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => optional($this->user)->name,
            'user_id' => $this->user_id,
            'dish_id' => $this->dish_id,
            'number_dish' => $this->number_dish,
            'total_amount' => $this->total_amount
        ];
    }
}
