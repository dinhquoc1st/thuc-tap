<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Area;
class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    

    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'address' => $this->address,
            'area_name' => optional($this->area)->name,
            'type' => $this->type,
            'time_start' => $this->time_start,
            'time_end' => $this->time_end
        ];
    }
}
