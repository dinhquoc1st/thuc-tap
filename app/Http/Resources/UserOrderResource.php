<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Helpers\Helper;
class UserOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => optional($this->user)->name,
            'name_dish' => optional($this->dish)->name,
            'number_dish' => $this->number_dish,
            'total_amount' => $this->total_amount,
            'date_order' => Helper::formatDateTime($this->created_at)
        ];
    }
}
