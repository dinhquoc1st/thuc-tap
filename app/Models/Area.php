<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;

    protected $table = "areas";

    protected $fillable = [
        'name',
        'ship_fee'

    ];
    public function dish_area()
    {
        return $this->hasMany(Store::class);
    }
    
}
