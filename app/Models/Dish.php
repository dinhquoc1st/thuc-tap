<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Arg;

class Dish extends Model
{
    use HasFactory;

    protected $table = "dishs";

    protected $fillable = [
        'name',
        'price',
        'store_id'

    ];
    public function dishes()
    {
        return $this->belongsTo(UserDish::class);
    }
    public function area()
    {
        return $this->belongsTo(Area::class);
    }
    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
