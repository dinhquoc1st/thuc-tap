<?php

return [
    'login' => [
        'success' => 'Login Success',
        'error' => 'Login Error'
    ],
    'area' => [
        'success' => [
            'delete' => 'Delete area success',
        ]
    ],
    'discount_code' => [
        'success' => [
            'delete' => 'Delete discount_code success',
        ]
    ],
    'dish' => [
        'success' => [
            'delete' => 'Delete dish success',
        ]
    ],
    'store' => [
        'success' => [
            'delete' => 'Delete store success',
        ]
    ]


];
