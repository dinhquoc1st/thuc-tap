<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\AreaController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\DishsController;
use App\Http\Controllers\API\DiscountController;
use App\Http\Controllers\API\StoreController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('auth/register', [AuthController::class, 'register']);
Route::post('auth/login', [AuthController::class, 'login']);
Route::group([
    'middleware' => 'admin',
    'prefix' => 'admin'
], function () {
    Route::group([
        'prefix' => 'users'
    ], function () {
        Route::post('create', [UserController::class, 'create'])->name('admin.user.getLogin');
        Route::get('show', [UserController::class, 'show']);
        Route::delete('delete/{id}', [UserController::class, 'delete']);
        Route::put('update/{id}', [UserController::class, 'update']);
        Route::get('detail/{id}', [UserController::class, 'detailUser']);
    });
    Route::group([
        'prefix' => 'dish'
    ], function () {
        Route::get('/', [DishsController::class, 'list']);
        Route::post('create', [DishsController::class, 'store']);
        Route::get('show/{id}', [DishsController::class, 'show']);
        Route::put('update/{id}', [DishsController::class, 'update']);
        Route::delete('delete/{id}', [DishsController::class, 'destroy']);
    });
    Route::group([
        'prefix' => 'area'
    ], function () {
        Route::get('/', [AreaController::class, 'list']);
        Route::post('create', [AreaController::class, 'create']);
        Route::get('show/{id}', [AreaController::class, 'show']);
        Route::put('update/{id}', [AreaController::class, 'update']);
        Route::delete('delete/{id}', [AreaController::class, 'destroy']);
    });
    Route::group([
        'prefix' => 'store'
    ], function () {
        Route::get('/', [StoreController::class, 'list']);
        Route::post('create', [StoreController::class, 'create']);
        Route::get('show/{id}', [StoreController::class, 'show']);
        Route::put('update/{id}', [StoreController::class, 'update']);
        Route::delete('delete/{id}', [StoreController::class, 'destroy']);
    });
    Route::group([
        'prefix' => 'discount'
    ], function () {
        Route::get('/', [DiscountController::class, 'list']);
        Route::post('create', [DiscountController::class, 'create']);
        Route::get('show/{id}', [DiscountController::class, 'show']);
        Route::put('update/{id}', [DiscountController::class, 'update']);
        Route::delete('delete/{id}', [DiscountController::class, 'destroy']);
    });
});
Route::group([
    'prefix' => 'users',
    'middleware' => 'auth-api'
], function () {
    Route::get('user-info', [AuthController::class, 'getProfile']);
    Route::put('update-user', [AuthController::class, 'updateProfile']);
});
